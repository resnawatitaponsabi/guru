<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Http\Requests\KaryawanRequest;
class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //public function index(Request $request)
    //{
        public function index(Request $request)
    {
        $keyword = $request->keyword;
        //$datas = Pegawai::all();
        $datas = Karyawan::where('nama', 'LIKE', '%'.$keyword.'%')
            ->orWhere('gelar', 'LIKE', '%'.$keyword.'%')
             ->orWhere('nip', 'LIKE', '%'.$keyword.'%')
            ->paginate();
         $datas->withPath('karyawan');
         $datas->appends($request->all());
        return view('Karyawan.index', compact(
            'datas' , 'keyword'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Karyawan;
        return view('Karyawan.create', compact(
            'model'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KaryawanRequest $request)
    {
        $model = new Karyawan;
        $model->nip = $request->nip;
        $model->nama = $request->nama;
        $model->tanggal_lahir = $request->tanggal_lahir;
        $model->gelar = $request->gelar;
        
        // $model->foto_profile = $request->foto_profile;
        //kita akan membuat code untuk upload file
        if($request->file('foto_profile')){
            $file = $request->file('foto_profile');
            $nama_file = time().str_replace(" ", "", $file->getClientOriginalName());
            $file->move('foto', $nama_file);
            $model->foto_profile = $nama_file;
        }
        $model->save();

        return redirect('karyawan')->with('success', "Data berhasil disimpan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Karyawan::find($id);
        return view('Karyawan.edit', compact(
            'model'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KaryawanRequest $request, $id)
    {
        $model = Karyawan::find($id);
        $model->nip = $request->nip;
        $model->nama = $request->nama;
        $model->tanggal_lahir = $request->tanggal_lahir;
        $model->gelar = $request->gelar;
        
        if($request->file('foto_profile')){
            $file = $request->file('foto_profile');
            $nama_file = time().str_replace(" ", "", $file->getClientOriginalName());
            $file->move('foto', $nama_file);

            File::delete('foto/'.$model->foto_profile);
            $model->foto_profile = $nama_file;
        }

        $model->save();

        return redirect('karyawan')->with('success', "Data berhasil diperbaharui");
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Karyawan::find($id);
        $model->delete();
        return redirect('karyawan');
    }
}
