
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <title>DASHBOARD | PEGAWAI</title>
  @include('Template.head')

  
</head>
<body class="hold-transition sidebar-mini" onload="realtimeClock()">
<div class="wrapper">

  <!-- Navbar -->
  
  <!-- /.navbar -->
  
  <!-- Main Sidebar Container -->
  @include('Template.left-sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Presensi Izin</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
       <h1>echa</h1>
       <div class="row justify-content-center">
        <div class="card card-info card-outline">
            <div class="card-header">Presensi Izin</div>
            <div class="card-body">
                <form action="{{ route('presensi-izin') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <center>
                        <label id="clock" style="font-size: 100px; color: #0A77DE; -webkit-text-stroke: 3px #00ACFE;
                                    text-shadow: 4px 4px 10px #36D6FE,
                                    4px 4px 20px #36D6FE,
                                    4px 4px 30px#36D6FE,
                                    4px 4px 40px #36D6FE;">
                        </label>
                    </center>
                    </div>
                    <center>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Klik Untuk Izin</button>
                           
                        </div>
                    </center>
                </form>

            </div>
        </div>
    </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('Template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
@include('Template.script')
</body>
</html>
