<aside class="main-sidebar sidebar-dark-primary elevation-4" >
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/echa1.jpg" alt="Admin" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Dashboard </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex" >
        <div class="image">
          <img src="dist/img/echa1.jpg" class="img-circle elevation-2" alt="echa1">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ auth()->user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                <a href="/home" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    DASHBOARD
                  </p>
                </a>
              </li>
              @if (auth()->user()->level == "karyawan")
          <li class="nav-item has-treeview ">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Presensi
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('presensi-masuk') }}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>PRESENSI MASUK</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('presensi-keluar') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>PRESENSI KELUAR</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          <li class="nav-item has-treeview ">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @if (auth()->user()->level == "karyawan")
              <li class="nav-item">
                <a href="/index" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>presensi per karyawan</p>
                </a>
              </li>
              @endif
              @if (auth()->user()->level == "admin")
              <li class="nav-item">
                <a href="{{url('filter-data/{tglawal}/{tglakhir')}} " class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>presensi keseluruhan</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @if (auth()->user()->level == "admin")
          <li class="nav-item">
            <a href="{{url('karyawan')}} " class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
               karyawan
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{route('presensi-izin')}} " class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                coba
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>