@extends('Form.index')

@section('content')
    <br/>
    <form method="POST" action="{{ url('karyawan/'.$model->id) }}" enctype="multipart/form-data">
        @csrf 
        <input type="hidden" name="_method" value="PATCH">

        @include('karyawan._form')
    </form>
@endsection