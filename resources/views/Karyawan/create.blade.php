@extends('Form.index')

@section('content')
<br/>
<form method="POST" action="{{ url('karyawan') }}" enctype="multipart/form-data">
    @csrf 
    @include('Karyawan._form')
</form>

@endsection