
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <title>DASHBOARD | PEGAWAI</title>
  @include('Template.head')
  
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
       
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h5>Absensi</h5>
       
      </li>
      
    </ul>

   

    
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('Template.left-sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12" style="background: #006400" >
            <h1 class="m-0 text-white">Selamat Datang Di</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      @if(Session::has('success'))
      <p class="alert alert-success">{{ Session::get('success') }}</p><br/>
  @endif    
  
  <a class="btn btn-info" href="{{ url('karyawan/create') }}">Tambah</a>
  <br/><br/>
  <form method="GET" action="{{ url('karyawan') }}">
      <input type="text" name="keyword" value="{{ $keyword }}" />
      <button type="submit">Search</button>
  </form>
  <br/>
  <table class="table-bordered table">
      <tr class="text-center">
          <th>Foto Profile</th>
          <th>NIP</th>
          <th>Nama</th>
          <th>Tanggal Lahir</th>
          <th>Gelar</th>
          <th colspan="2">AKSI</th>
      </tr>
      @foreach($datas as $key=>$value)
          <tr>
              <td>
                  @if(strlen($value->foto_profile)>0)
                      <img src="{{ asset('foto/'.$value->foto_profile) }}" width=50px />
                  @endif
              </td>
              <td>{{ $value->nip }}</td>
              <td>{{ $value->nama }}</td>
              <td>{{ $value->tanggal_lahir }}</td>
              <td>{{ $value->gelar }}</td>
              <td class="text-center"><a class="btn btn-info" href="{{ url('karyawan/'.$value->id.'/edit') }}">Update</a></td>
              <td class="text-center">
                  <form action="{{ url('karyawan/'.$value->id) }}" method="POST">
                      @csrf 
                      <input type="hidden" name="_method" value="DELETE">
                      <button class="btn btn-danger" type="submit">DELETE</button>
                  </form>
              </td>
          </tr>
      @endforeach
  </table>
  {{ $datas->links() }}
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('Template.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
@include('Template.script')
</body>
</html>
